<?php
/** 
 *------------------------------------------------------------------------------
 * @package       T3 Framework for Joomla!
 *------------------------------------------------------------------------------
 * @copyright     Copyright (C) 2004-2013 JoomlArt.com. All Rights Reserved.
 * @license       GNU General Public License version 2 or later; see LICENSE.txt
 * @authors       JoomlArt, JoomlaBamboo, (contribute to this project at github 
 *                & Google group to become co-author)
 * @Google group: https://groups.google.com/forum/#!forum/t3fw
 * @Link:         http://t3-framework.org 
 *------------------------------------------------------------------------------
 */

 
// no direct access
defined('_JEXEC') or die;
?>

<div class="span4 col-md-4">
	<div class="tpl-preview">
    <img src="<?php echo T3_TEMPLATE_URL ?>/template_preview.png" alt="Template Preview" />
	</div>
</div>
<div class="span8 col-md-8">
	<div class="t3-admin-overview-header">
  	<h2>
      Eatoreh
      <small style="display: block;">Eatoreh is an outstanding Joomla templates 2.5 & 3.1</small>
    </h2>
    <p>Eatoreh is a legendary joomla templates on themeforest, it's now rebuilt to adapt to the latest web technology. Fully responsive and optimized for tablet and any others mobile screen, integrated with T3 bootstrap 3.x and Font Awesome 4.0.x. Wide container is added. Replace many modules with tha latest technology. Eatoreh is Clean and elegance looked design, it's suitable to meet with any website demographics. Aside from just looking beautifully polished and clean, 10 colour options, a nice slideshow module from Gavick, easily configured various modules positions, a newsletter plugin, and a lot other best and selected extensions. So, If you're looking for a fresh new Joomla templates, than look no further. This templates is a powerful Joomla template for your websites project.</p>
	</div>
	<div class="t3-admin-overview-body">
    <h4>Resources:</h4>
    <ul class="t3-admin-overview-features">
      <li><a href="http://joomla.themesoul.com/eatoreh/" title="Demo Link">Demo Link</a></li>
	   <li><a href="http://themesoul.com/support/" title="Forum Link">Forum Link</a></li>
    </ul>
	</div>
</div>