<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php if ($this->countModules('franja')) : ?>       
    <jdoc:include type="modules" name="franja" style="none" />
<?php endif; ?>
<!-- NEWSLETTER -->
<?php if ($this->countModules('newsletter')) : ?>       
<section class="t3-sl-1 t3-newsletter">
  <div class="container">
    		<jdoc:include type="modules" name="<?php $this->_p('newsletter') ?>" style="none" />
  </div>
</section>
<?php endif; ?>

<!-- //NEWSLETTER -->

<!-- FOOTER -->
<footer id="t3-footer" class="wrap t3-footer">

		<!-- FOOT NAVIGATION -->
		<div class="container">
      	<div class="row">
  	      <?php if ($this->countModules('user6')) : ?>       
      		<div class="col-md-5 col-sm-5">
    				<jdoc:include type="modules" name="<?php $this->_p('user6') ?>" style="T3Xhtml" />
      		</div>
         	<?php endif; ?>

         	<?php if ($this->checkSpotlight('footnav', 'user7, user8, user9, user10')) : ?>
            <div class="col-md-7 col-sm-7">
					<?php $this->spotlight('footnav', 'user7, user8, user9, user10') ?>
            </div>
         	<?php endif ?>
        </div>
		</div>
		<!-- //FOOT NAVIGATION -->

		<div class="container">
			<div class="t3-copyright">
				<div class="<?php echo ($this->getParam('t3-rmvlogo', 1) or $this->countModules('social')) ? 'col-md-8 col-sm-6 col-xs-12' : 'col-md-12' ?> copyright <?php $this->_c('footer') ?>">
					<jdoc:include type="modules" name="<?php $this->_p('footer') ?>" />
          <!--<small>
            <a href="http://twitter.github.io/bootstrap/" target="_blank">Bootstrap</a> is a front-end framework of Twitter, Inc. Code licensed under <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a>.
          </small>
          <small>
            <a href="http://fortawesome.github.io/Font-Awesome/" target="_blank">Font Awesome</a> font licensed under <a href="http://scripts.sil.org/OFL">SIL OFL 1.1</a>.
          </small>-->
				</div>
				<?php if ($this->countModules('social') or $this->getParam('t3-rmvlogo', 1)): ?>
					<div class="col-md-4 col-sm-6 col-xs-12 poweredby text-hide">
          	<?php if ($this->countModules('social')) : ?>      
          	<jdoc:include type="modules" name="<?php $this->_p('social') ?>" />
            <?php endif; ?>
            <?php if ($this->getParam('t3-rmvlogo', 1)): ?>
						<a class="t3-logo t3-logo-color" href="http://t3-framework.org" title="Powered By T3 Framework" target="_blank" rel="nofollow">Powered by <strong>T3 Framework</strong></a>
            <?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>

</footer>
<!-- //FOOTER -->