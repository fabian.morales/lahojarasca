<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<?php if ($this->countModules('sliderload')) : ?>
<!-- SLIDESHOW -->
<section class="t3-sl wrap t3-slider">
	<div class="container">

    <?php if($this->countModules('sliderload')) : ?>
      <jdoc:include type="modules" name="<?php $this->_p('sliderload') ?>" style="none" />
    <?php endif; ?>

	</div>
</section>
<!-- //SLIDESHOW -->
<?php endif ?>


<?php if ($this->countModules('gmaps')) : ?>
<!-- Google Maps -->
<section class="wrap t3-gmaps">
	<jdoc:include type="modules" name="<?php $this->_p('gmaps') ?>" style="none" />
</section>
<!-- //Google Maps -->
<?php endif ?>