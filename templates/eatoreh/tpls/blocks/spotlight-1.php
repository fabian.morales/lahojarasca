<?php
/**
 * @package   T3 Blank
 * @copyright Copyright (C) 2005 - 2012 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<?php if ($this->countModules('promo')) : ?>
<section class="t3-sl t3-sl-2 wrap t3-promo">
	<div class="container">
  	<div class="row">
    		<jdoc:include type="modules" name="<?php $this->_p('promo') ?>" style="none" />
    </div>
	</div>
</section>
<?php endif ?>


<?php if ($this->checkSpotlight('spotlight-1', 'position-1, position-2, position-3, position-4')) : ?>
	<!-- SPOTLIGHT 1 -->
	<div class="container t3-sl">
		<?php $this->spotlight('spotlight-1', 'position-1, position-2, position-3, position-4') ?>
	</div>
	<!-- //SPOTLIGHT 1 -->
<?php endif ?>